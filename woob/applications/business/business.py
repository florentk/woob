# -*- coding: utf-8 -*-


from __future__ import print_function

import os
import hashlib
import csv
import codecs
import time
import uuid
import json

from datetime import datetime
from time import localtime, strftime

from woob.capabilities.commercial import CapDocument, Document, CapLine, Line
from woob.capabilities.customer import CapCustomer, Customer
from woob.capabilities.expense import Expense, CapExpense, Line as ExpenseLine, CapLine as CapExpenseLine

from woob.tools.application.repl import ReplApplication, defaultcount
from woob.tools.application.formatters.iformatter import IFormatter

from decimal import Decimal

from colorama import Fore, Back, Style

__all__ = ['Business']

CLIENT_EXPORT_HEADER = ["nom","prenom","adresse number","adresse","adresse complement","code postal","ville","tel","email","cat","memo","erreur"]
DOCUMENT_EXPORT_HEADER = WOOB_HEADER = ["Account","Title","Date","Payment choice","Payment due ID","Code article","Description","Price Unit","Including tax","Tax rate","Unit ID","Quantity","Amount","Amount VAT","Activity ID","Memo","erreur"]


class DocumentListFormatter(IFormatter):
    MANDATORY_FIELDS = ()
    count = 0

    def flush(self):
        self.count = 0

    def format_obj(self, obj, alias):
        result = self.format_document(obj)
        return result

    def format_document(self, doc):

        self.count += 1


        result = u'%d %s %s %s %s %s %s %s %s %s %s %s %s %s %s' % (self.count,doc.id,doc.date,doc.label,doc.type,doc.id_chrono,doc.amount,doc.amount_vat,doc.amount_to_paid,doc.currency,doc.duedate,doc.state,doc.pending,doc.customer,doc.customer_num)

        return result

class Business(ReplApplication):
    APPNAME = 'business'
    VERSION = '1.1'
    COPYRIGHT = 'Copyright(C) 2018-YEAR Florent Kaisser'
    DESCRIPTION = "Console application allowing to interact with an commercial management application"
    SHORT_DESCRIPTION = "commercial management"
    CAPS = CapDocument
    EXTRA_FORMATTERS = {'doclist':  DocumentListFormatter
                       }
    COMMANDS_FORMATTERS = {'list':          'doclist'
                          }

    def load_default_backends(self):
        self.load_backends(CapDocument, storage=self.create_storage())

    def main(self, argv):
        self.load_config()
        return ReplApplication.main(self, argv)

    @defaultcount(10)
    def do_list(self, arg):
        """
        list

        Display all documents
        """
        for doc in self.do('iter_documents'):
            self.format(doc)

    @defaultcount(10)
    def do_search(self, arg):
        """
        list text

        Search  documents
        """
        print("Recherche " + arg)
        for doc in  self.do('search_documents',arg):
            self.format(doc)



    @defaultcount(10)
    def do_customers(self, arg):
        """
        customers

        Display all customers
        """
        
        self.start_format()
        for cust in self.do('iter_customers', caps=CapCustomer):
            self.format(cust)

    @defaultcount(10)
    def do_expenses(self, args):
        """
        expenses
        
        Display all expenses
        """
        
        self.start_format()
        for exp in self.do('iter_expenses', caps=CapExpense):
            self.format(exp)

    @defaultcount(10)
    def do_expense_lines(self, id):
        """
        expense_lines

        Display all lines of a expense note
        """
        
        self.start_format()
        for line in self.do('iter_expenses_lines', id, caps=CapLine):
            self.format(line)

    def do_structures(self, args):
        """
        structures
            
        List structures avaible for the current user
        """

        for struct in self.do('iter_structure'):
            print(struct['id'],struct['nom'])

    def do_change_structure(self, id):
        """
        change_structure
            
        Change the current structure
        """
        
        for struct in  self.do('change_structure', id):
            print("Structure courante : %s" % struct)

    @defaultcount(10)
    def do_lines(self, id):
        """
        lines

        Display all lines of a document
        """
        
        self.start_format()
        for line in self.do('iter_lines', id, caps=CapLine):
            self.format(line)

    def do_edit(self, args):
        """
        edit

        Edit a line
        """
        (pieceId, lineId) = args.split()
        
        self.do('edit_document', pieceId, caps=CapLine)
        line = self.get_object(lineId, 'edit_line')
        line.description = "toto"
        self.do('valid_line')
        self.do('valid_document')

    def download(self, pieceId, dest=None):
        if dest is None:
            dest = "%s.pdf" % (pieceId)
        print("Téléchargement de la pièce n°%d" % (pieceId))
        for buf in self.do('download_PDF', pieceId, caps=CapLine):
            if buf:
                if dest == "-":
                    if sys.version_info.major >= 3:
                        self.stdout.buffer.write(buf)
                    else:
                        self.stdout.stream.write(buf)
                else:
                    try:
                        with open(dest, 'wb') as f:
                            f.write(buf)
                    except IOError as e:
                        print('Impossible de télécharger la pièce dans "%s": %s' % (dest, e), file=self.stderr)
                        return 1
                return

    def do_download(self, line):
        """
        download

        download a document in PDF
        """
        pieceId, dest = self.parse_command_args(line, 2, 1)
        self.download(pieceId, dest)



    def do_document(self, accountId):
        """
        create

        create a new document
        """
        self.do('create_document', accountId)
        self.do('create_line')
        self.do('valid_line')
        self.do('submit_document')

    def do_create_line(self, id):
        """
        create

        create a line
        """
        
        self.do('edit_document', id, caps=CapLine)
        self.do('create_line')
        self.do('valid_line')
        self.do('submit_document')

    def do_create_expense(self, args):
        """
        create_expense

        create a expence note
        """
        (titre) = args.split()

        self.do('create_expense', titre)
        self.do('create_expense_line', -1, caps=CapExpenseLine)
        self.do('valid_expenses_line')
        self.do('submit_expense')

    def do_create_expense_line(self, id):
        """
        create_expense_line

        create a line in a expense note
        """

        self.do('edit_expense', id, caps=CapExpenseLine)
        self.do('create_expense_line', -1, caps=CapExpenseLine)
        self.do('valid_expenses_line')
        self.do('submit_expense')

    def do_create_client(self, line):
        """
        create_client

        add a new client
        """
        #{"keys":[],"value":{"id":null,"nom":"TEST","prenom":"CLIENT","titreId":302,"categorieId":1,"memo":"client de test","enSommeil":false,"numeroClient":null,"activiteAutorise":{"recherche":null,"voirQueSiAutorise":false,"touteActivite":true,"activites":[]},"clientComptes":null,"suffixe":null,"adresses":[{"id":null,"codePostal":"59000","commune":{"id":18651,"nom":"LILLE","code":"59000"},"pays":null,"numero":"20","adresse1":null,"adresse2":null,"adresse3":"avenue de la république","adresse4":null,"typAdresseId":1}],"telephones":[{"id":null,"numeroTel":"0601020304","telPrincipal":true,"memoTel":"protable"}],"courriels":[{"id":null,"adresseMel":"toto@titit.com","memoMel":"pro","melPrincipal":true}],"ibans":[],"libNaf":null,"codeNaf":null,"clientIbans":null,"numeroTva":null,"numeroSiret":null,"echeanceReglement":3,"modeReglement":529,"numeroNaf":null,"contacts":[{"id":null,"nom":"paul","prenom":"jacque","titreId":300,"memo":null,"adresses":null,"telephones":[{"id":null,"numeroTel":"0608090704","telPrincipal":true,"memoTel":"principa"}],"courriels":null}],"duplication":false}}

        #(catId, nom, prenom, adresse, code, ville) = self.parse_command_args(line, 5, 5)
        #catId = "1"
        #client = self.get_empty_client(int(catId))

        #client['value']['nom'] = nom
        #client['value']['prenom'] = prenom
        #client['value']['adresse'] = adresse
        #client['value']['code'] = code
        #client['value']['ville'] = ville

        #print("ajout du client %s %s" % (client['value']['nom'], client['value']['prenom']))

        #self.do('create_client', client)
        #time.sleep(2)
       # client = self.get_empty_client(int(catId))
        #self.do('create_client', client)
        return 0

    def writeImportErrors(self, rows):
        n = len(rows)
        if(n > 1):
            file_name = "import_error_%s.csv" % datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            with codecs.open(file_name, 'w', encoding='utf-8') as f:
                writer = csv.writer(f, delimiter=';')
                writer.writerows(rows)
            print(Back.RED)
            print("%d entrées avec erreur(s). Voir fichier %s " % (n-1, file_name))
            print(Style.RESET_ALL)

    def create_client(self, _id, nom, prenom, adresse_num, adresse, adresse_ex, code, ville, category, email, tel, memo, force):
        client = self.get_object(str(_id), 'create_client')

        #needed fields
        if(nom):
          client.name = nom
        if(adresse):
          client.street_name = adresse
        if(code):
          client.postal_code = code
        if(ville):
          client.city = ville
        if(prenom):
          client.firstname = prenom
        if(category):
          client.category = category

        #optional fields
        if(category != "individual"):
          client.firstname = prenom
        client.street_number = adresse_num
        client.street_extra = adresse_ex
        client.email = email
        client.telephone = tel
        client.memo = memo

        for res in self.do('save_client', force):
            return  res

    def get_error_msg(self, e):
        return str(e.errors[-1][1].args[0])


    def do_import_client(self, line):
        """
        import_client CSV_FILE [force]
        
        Import clients from a CSV file
        """
        clients_error = [CLIENT_EXPORT_HEADER]

        csv_file, force = self.parse_command_args(line, 2, 1)

        #count the number of line
        c = 0
        with open(csv_file, 'r') as f:
            for l in f:
                if l.strip():
                    c = c + 1

        nb_entry = c - 1 #don't count the header
        print("Import from %s (%d entries)" % (csv_file, nb_entry))

        if(csv):
            with codecs.open(csv_file, 'r') as f:
                reader = csv.reader(f,delimiter=';', quotechar='"')
                next(reader) # ignore first line
                _id = 0
                self.get_object(str(_id), 'init_create_client')
                for row in reader:
                    nom = row[0].strip()
                    prenom = row[1].strip()
                    adresse_num = row[2].strip()
                    adresse = row[3].strip()
                    adresse_ex = row[4].strip()
                    code = row[5].strip()
                    ville = row[6].strip()
                    tel = row[7].strip()
                    email = row[8].strip()
                    category = row[9].strip()
                    memo = row[10].strip()

                    _id = _id + 1
                    try:
                        self.create_client(_id, nom, prenom, adresse_num, adresse, adresse_ex, code, ville, category, email, tel, memo, force != None)
                        print(Back.GREEN)
                        print("[%d/%d] Client créé : %s %s [%d]" % (_id, nb_entry,client.name, client.firstname, client.id))
                        print(Style.RESET_ALL)
                    except Exception as e:
                        print(Back.RED)
                        err = self.get_error_msg(e)
                        print("[%d/%d] Erreur : %s" % (_id, nb_entry, err) )
                        print(Style.RESET_ALL)
                        row.append(err)
                        clients_error.append(row)
                    time.sleep(0.5)
                self.writeImportErrors(clients_error)
    def do_edit_expense(self, args):
        """
        edit_expense

        Edit a line in a expense note
        """
#        (expId, lineId) = args.split()
#        
#        self.get_object(expId,'edit_expense')
#        line = self.get_object(lineId, 'edit_expenses_line')
#        line.description = "Site web"
#        line.date = datetime.date(2020, 3, 1)
#        line.supplier_description = "OVH"
#        line.supplier_id = -1
#        line.purchase_type_id = 2253
#        line.amount = 20.0
#        line.vat = 4.0
#        self.do('valid_expenses_line', caps=CapExpenseLine)
#        self.do('submit_expense')
        return 0

    def do_import_expense(self, line):
        """
        import_expense

        Import a expence note from a CSV file
        """

        csv_file, validation, individu, activity = self.parse_command_args(line, 4, 1)

        if(csv):
            transmit = False
            valid = False
            submit = True
            if(validation):
              if(validation=="transmit"):
                transmit = True;
              if(validation=="valid"):
                valid = True;
              if(validation=="nosubmit"):
                submit = False

            _id = 0
            prev_expence_id = -1
            with codecs.open(csv_file, 'r') as f:
                reader = csv.reader(f,delimiter=';', quotechar='"')
                next(reader) # ignore first line
                for row in reader:
                    _id = _id + 1

                    matricule = row[0]
                    utilisateur = row[1]
                    expence_id = row[2]
                    date = row[3]
                    if(row[4]):
                      commentaire = row[4]
                    else:
                      commentaire = "N/A"
                    fournisseur = row[5]
                    nature = row[6]
                    activite_a_imputer = row[7]
                    montant_ht = row[8]
                    tva = row[9]
                    try:

                        if(prev_expence_id != expence_id):
                            if(_id > 1):
                                #one line or more has been added => we can submit the expense
                                if(submit):
                                  self.do('submit_expense')
                                if(transmit):
                                  self.do('transmit_expense')
                                if(valid):
                                  self.do('valid_expense')
                                time.sleep(5)

                            print('Begin import of a new expense')
                            self.do('iter_expenses')
                            current_date = strftime("%d%m%Y", localtime())
                            title = "Frais Cleemy n°%s - %s - Vérifié" % (expence_id,current_date)
                            self.get_object(title,'create_expense')
                            if(individu):
                                self.get_object(individu,'set_expense_person')
                            else:
                                self.get_object(utilisateur,'set_expense_person')
                            if(activity):
                                self.get_object(activity,'set_expense_activity')
                            _id = 1
                            prev_expence_id = expence_id

                        line = self.get_object(str(_id),'create_expense_line')
                        line.date = datetime.strptime(date,'%d/%m/%Y')
                        line.description = commentaire
                        line.supplier_description = fournisseur
                        line.purchase_type = nature
                        line.amount = Decimal(montant_ht.replace(',','.'))
                        line.vat = Decimal(tva.replace(',','.'))

                        #if activité à imputé est défini, alors n'utilise pas l'activité par défaut pour cette ligne
                        if(activite_a_imputer):
                          #line.activity_code = activite_a_imputer
                          #line.activity_id = -1 #défini l'activité par code et non par Id
                          self.get_object(activite_a_imputer,'set_expense_line_activity_charged')
                        
                        self.get_object(str(line.purchase_id),'valid_expenses_line')
                    except Exception as e:
                        print(Back.RED)
                        print("ERROR : " + self.get_error_msg(e))
                        print(Style.RESET_ALL)

                if(submit):
                  self.do('submit_expense')
                if(transmit):
                  self.do('transmit_expense')
                if(valid):
                  self.do('valid_expense')

    def action_document(self, action, title, download_path):
        for pieceId in self.do('%s_document' % action):
            if(download_path):
                self.download(pieceId, "%s/%s.pdf" % (download_path,title))
            return pieceId

    def do_import_document(self, line):
        """
        import_document CSV_FILE [transmit|valid|nosubmit] FACTURE_PATH

        Import a document from a CSV file
        """
        
        def show_error(err):
            print(Back.RED)
            print("Erreur : %s" % (err) )
            print(Style.RESET_ALL)
        
        def action_document(submit,transmit,valid,nb_line,title,download_path):
            docId = -1
            #more of one line in expence
            if(submit):
                docId = self.action_document('submit', title, None)
            if(transmit):
                docId = self.action_document('transmit', title, download_path)
            if(valid):
                docId = self.action_document('valid', title, download_path)
            print(Back.GREEN)
            print("Facture créée : %s avec %d ligne(s) [Pièce n°%d]" % (title, nb_line, docId))
            print(Style.RESET_ALL)
            time.sleep(2)

        document_error = [DOCUMENT_EXPORT_HEADER]
        #print(line)
        csv_file, validation, download_path = self.parse_command_args(line, 3, 1)

        #print("Import depuis %s" % (csv_file))

        #count the number of line
        c = 0
        with open(csv_file, 'r') as f:
            for l in f:
                if l.strip():
                    c = c + 1

        nb_entry = c - 1 #don't count the header
        c_entry = 0

        if(nb_entry < 1):
            return

        transmit = False
        valid = False
        submit = True
        if(validation):
          if(validation=="transmit"):
            transmit = True;
          if(validation=="valid"):
            valid = True;
          if(validation=="nosubmit"):
            submit = False

        with codecs.open(csv_file, 'r') as f:
            reader = csv.reader(f,delimiter=';', quotechar='"')
            next(reader) # ignore first line
            prev_account_id = ""
            prev_title = ""
            prev_date = ""
            _id = 0
            last_row = None
            submit_document = True
            add_line = True
            for row in reader:

                last_row = row

                # get row (on line of invoice)
                account_id = row[0]
                title = row[1]
                date = row[2]
                payment_choice = row[3]
                payment_due = row[4]
                article_code = row[5]
                description = row[6]
                price_unit = Decimal(row[7].replace(',', '.'))
                including_tax = (row[8] == '1')
                tax_rate = Decimal(row[9].replace(',', '.'))
                unit_id = row[10]
                quantity = Decimal(row[11].replace(',', '.'))
                amount = Decimal(row[12].replace(',', '.'))
                amount_vat = Decimal(row[13].replace(',', '.'))
                activity_id = row[14]
                memo = row[15].replace('\\n', '\n')
                first_name = row[16]
                last_name = row[17]
                addr_num = row[18]
                address = row[19]
                addr_ex = row[20]
                zip_code =  row[21]
                city = row[22]
                tel = row[23]
                category = row[24]
                client_id = row[25]
                #memo_piece = row[26]

                c_entry = c_entry + 1

                try:
                    if( not ((prev_account_id == account_id) and (prev_title == title) and (prev_date == date))):
                      add_line = True
                      if(_id > 0 and submit_document):
                          # submit / valid / transmit doc
                          action_document(submit, transmit, valid, _id, prev_title, download_path)

                      print ("Début d'une nouvelle facture")
                      #TODO test id document exist with title 

                      pieceId = -1
                      for d in self.do('search_documents',title):
                          pieceId = int(d.id)

                      if pieceId == -1:
                          try:
                              self.get_object(account_id, 'create_document')
                          except Exception as e:
                              print(Back.YELLOW)
                              print("%s" % self.get_error_msg(e))
                              print(Back.GREEN)
                              print("Ajout du client manquant")
                              print(Style.RESET_ALL)
                              # client do not exist ! Create it
                              self.get_object("-1", 'init_create_client')
                              self.create_client(-1, first_name, last_name, addr_num, address, addr_ex, zip_code, city, category, "", tel, client_id, False)
                              # retry create document
                              self.get_object(account_id, 'create_document')
                          self.get_object(title, 'set_document_title')
                          self.get_object(payment_choice, 'set_document_payement_mode') #2185
                          self.get_object(payment_due, 'set_document_payement_due')
                          #self.get_object(memo_piece, 'set_document_memo')
                          #self.get_object(date, 'set_document_date')
                          _id = 0
                          submit_document = True
                      else:
                          err = "Facture %s déjà existante" % title
                          show_error(err)
                          row.append(err)
                          document_error.append(row)
                          add_line = False
                          submit_document = False
                          self.download(pieceId, "%s/%s.pdf" % (download_path,title))

                    if(add_line):
                        prev_account_id = account_id
                        prev_title = title
                        prev_date = date

                        # create the new line
                        _id = _id + 1
                        line = self.get_object(article_code,'create_line')
        #                line.id = _id
                        line.description = description
                        line.price_unit = price_unit
                        line.unit_id = unit_id
                        line.quantity = quantity
                        line.amount = amount
                        line.amount_vat = amount_vat
                        line.vat = including_tax
                        line.vat_rate = tax_rate

                        if(activity_id):
                            line.activity_id = activity_id
                        line.memo = memo
                        self.get_object(str(_id),'valid_line')
                        print("[%d/%d] Ligne de la facture %s ajoutée" % (c_entry,nb_entry,title))

                except Exception as e:
                    err = self.get_error_msg(e)
                    show_error(err)
                    if(last_row):
                        last_row.append(err)
                        document_error.append(last_row)
                    submit_document = False
            
            if(submit_document):
                try:
                    action_document(submit, transmit, valid, _id, prev_title, download_path)
                except Exception as e:
                    err = self.get_error_msg(e)
                    show_error(err)
                    if(last_row): 
                        last_row.append(err)
                        document_error.append(last_row)

            self.writeImportErrors(document_error)
