# -*- coding: utf-8 -*-


from woob.capabilities.base import BaseObject, StringField, DecimalField, BoolField, Currency, Field, UserError, IntField
from woob.capabilities.date import DateField
from woob.capabilities.collection import CapCollection

__all__ = ['DocumentNotFound', 'Document', 'CapDocument', 'Bill', 'Line', 'CapLine']


class DocumentNotFound(UserError):
    """
    Raised when an account is not found.
    """

    def __init__(self, msg='Document not found'):
        super(DocumentNotFound, self).__init__(msg)

class Document(BaseObject):
    """
    Document.
    """
    _id = StringField('ID', default=None)
    date =          DateField('The day the document has been established')
    label =         StringField('label of document', default="N/A")
    type =          StringField('type of document')


class Bill(Document, Currency):
    """
    Bill.
    """
    id_chrono = StringField('Chronological id', default="N/A")
    amount =         DecimalField('Amount of bill')
    amount_vat =         DecimalField('Amount of bill VAT')
    amount_to_paid =         DecimalField('Amount to be paid')
    currency =      StringField('Currency', default=None)
    duedate =       DateField('The day the bill must be paid')
    state =      StringField('Bill state', default="N/A")
    pending = BoolField('Bill is pending')
    customer = StringField('Customer', default="N/A")
    customer_num = StringField('Customer number', default="N/A")

class Line(BaseObject, Currency):
    """
    Line of a document
    """
    article_id = IntField('Article id', default=None)
    description = StringField('Description', default=None)
    vat_rate = DecimalField('VAT Rate')
    price_unit = DecimalField('Unit price')
    quantity = DecimalField('Quantity')
    unit_id = StringField('Unit', default=None)
    amount = DecimalField('Total price')
    amount_vat = DecimalField('Total price, taxes included')
    activity_id = StringField('Activity id', default=None)
    memo = StringField('Memo', default=None)
    vat = BoolField('Unit price, taxes included')
    currency = u'€'

class CapDocument(CapCollection):
    """
    Capability of see documents (bill or quotation)
    """

    def iter_resources(self, objs, split_path):
        """
        Iter resources.

        Default implementation of this method is to return on top-level
        all document (by calling :func:`iter_pending_payment`).

        :param objs: type of objects to get
        :type objs: tuple[:class:`BaseObject`]
        :param split_path: path to discover
        :type split_path: :class:`list`
        :rtype: iter[:class:`BaseObject`]
        """
        if Document in objs:
            self._restrict_level(split_path)

            return self.iter_documents()

    def iter_documents(self):
        """
        Iter pending payment.

        :rtype: iter[:class:`Bill`]
        """
        raise NotImplementedError()
        
    def download_PDF(self, pieceId):
        """
        download the document in PDF
        
        :param pieceId: ID of document
        :type pieceId: int

        :rtype: bytes
        :raises: :class:`DocumentNotFound`

        """
        raise NotImplementedError()

    def create_document(self,accountId):
        """
        create a new document
        
        :param accountId: ID of client's account
        :type accountId: int

        :rtype: :class:`Bill`
        """
        raise NotImplementedError()

    def edit_document(self, _id):
        """
        edit a document
        
        :param _id: ID of document
        :type _id: int

        :rtype: :class:`Bill`
        """
        raise NotImplementedError()

    def valid_document(self):
        """
        validate a document

        :rtype: :class:`Bill`
        """
        raise NotImplementedError()

class CapLine(CapCollection):
    """
    Capability of see documents (bill or quotation)
    """

    def iter_resources(self, objs, split_path):
        """
        Iter resources.

        Default implementation of this method is to return on top-level
        all document (by calling :func:`iter_lines`).

        :param objs: type of objects to get
        :type objs: tuple[:class:`BaseObject`]
        :param split_path: path to discover
        :type split_path: :class:`list`
        :rtype: iter[:class:`BaseObject`]
        """
        if Line in objs:
            self._restrict_level(split_path)

            return self.iter_lines()

    def iter_lines(self):
        """
        Iter document line.

        :rtype: iter[:class:`Line`]
        """
        raise NotImplementedError()
        
    def valid_line(self):
        """
        Valid the current line edition in the current document.

        :rtype: :class:`Line`
        """
        raise NotImplementedError()

    def edit_line(self, _id):
        """
        Edit a line in the current document.
        
        :param _id: ID of line
        :type _id: str

        :rtype: :class:`Line`
        """
        raise NotImplementedError()
        
    def create_line(self, articleCode):
        """
        Create a new line in the current document.
        
        :param articleCode: code of article
        :type articleCode: str

        :rtype: :class:`Line`
        """
        raise NotImplementedError()
    
    
