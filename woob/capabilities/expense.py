# -*- coding: utf-8 -*-


from woob.capabilities.base import BaseObject, StringField, DecimalField, BoolField, Currency, Field, UserError, IntField
from woob.capabilities.date import DateField
from woob.capabilities.collection import CapCollection

__all__ = ['ExpenseNotFound', 'Expense', 'CapExpense', 'Line', 'CapLine']


class ExpenseNotFound(UserError):
    """
    Raised when an account is not found.
    """

    def __init__(self, msg='ExpenseNotFound not found'):
        super(ExpenseNotFound, self).__init__(msg)

class Expense(BaseObject, Currency):
    """
    Expense note
    """
    expense_id = StringField('ID', default=None)
    date = DateField('The day the expense has been established')
    label = StringField('Label of the expense', default="N/A")
    id_chrono = StringField('Chronological number', default=None)
    third_party = StringField('Chronological number', default="N/A")
    amount_vat = DecimalField('Total amount')
    state = StringField('Chronological number', default="N/A")


class Line(BaseObject, Currency):
    """
    Line of a expense note
    """
    purchase_id = IntField('Purchase id', default=None)
    description = StringField('Description', default=None)
    position = IntField('Position in the expense', default=None)

    amount = DecimalField('Total price')
    vat = DecimalField('Vat price')
    date = DateField('The day of the purchase')

    purchase_type =  StringField('Purchase type description', default=None)
    
    supplier_description = StringField('Supplier description', default=None)
    supplier_id = IntField('Supplier id', default=None)

    activity_id = IntField('Activity id', default=None)
    #activity_code = StringField('Activity Code', default="")

class CapExpense(CapCollection):
    """
    Capability of see a expense note
    """

    def iter_resources(self, objs, split_path):
        """
        Iter resources.

        Default implementation of this method is to return on top-level
        all expense (by calling :func:`iter_expenses`).

        :param objs: type of objects to get
        :type objs: tuple[:class:`BaseObject`]
        :param split_path: path to discover
        :type split_path: :class:`list`
        :rtype: iter[:class:`BaseObject`]
        """
        if Document in objs:
            self._restrict_level(split_path)

            return self.iter_expenses()

    def iter_expenses(self):
        """
        Iter pending payment.

        :rtype: iter[:class:`Expense`]
        """
        raise NotImplementedError()
        
    def download_PDF(self, pieceId):
        """
        download the expense in PDF

        :param expenseId: ID of document
        :type expenseId: int

        :rtype: bytes
        :raises: :class:`ExpenseNotFound`

        """
        raise NotImplementedError()
        
    def create_expense(self, title):
        """
        create a new expense

        :param title: title of the expense

        :rtype: :class:`Expense`
        """
        raise NotImplementedError()

    def submit_frais(self):
        """
        Save the current expense.

        :rtype: :class:`Line`
        """
        raise NotImplementedError()


class CapLine(CapCollection):
    """
    Capability to see a line of a expense note
    """

    def iter_resources(self, objs, split_path):
        """
        Iter resources.

        Default implementation of this method is to return on top-level
        all document (by calling :func:`iter_lines`).

        :param objs: type of objects to get
        :type objs: tuple[:class:`BaseObject`]
        :param split_path: path to discover
        :type split_path: :class:`list`
        :rtype: iter[:class:`BaseObject`]
        """
        if Line in objs:
            self._restrict_level(split_path)

            return self.iter_expenses_lines()

    def iter_expenses_lines(self):
        """
        Iter expense line.

        :rtype: iter[:class:`Line`]
        """
        raise NotImplementedError()
        
    def valid_expenses_line(self):
        """
        Valid the current line in the current document.

        :rtype: :class:`Line`
        """
        raise NotImplementedError()

    def edit_expenses_line(self, _id):
        """
        Edit a line in the current expense.
        
        :param _id: ID of line
        :type _id: str

        :rtype: :class:`Line`
        """
        raise NotImplementedError()
        
    def create_expense_line(self, purchaseTypeId):
        """
        Create a new line in the expense.
        
        :param purchaseTypeId: id of purchase type id
        :type purchaseTypeId: str

        :rtype: :class:`Line`
        """
        raise NotImplementedError()

