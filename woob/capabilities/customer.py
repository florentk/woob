# -*- coding: utf-8 -*-


from woob.capabilities.base import BaseObject, StringField, DecimalField, BoolField, Currency, Field, UserError
from woob.capabilities.date import DateField
from woob.capabilities.collection import CapCollection

__all__ = ['CustomerNotFound', 'Customer', 'CapCustomer']


class CustomerNotFound(UserError):
    """
    Raised when an account is not found.
    """

    def __init__(self, msg='Customer not found'):
        super(CustomerNotFound, self).__init__(msg)

class Customer(BaseObject):
    """
    Customer.
    """
    
    _id = StringField('ID', default=None)
    title = StringField('Title', default="")
    name = StringField('Name', default="N/A")
    firstname = StringField('Firstname', default="")
    contactname = StringField('Contact name', default="")
    account_id = StringField('Account ID', default="")
    account_name = StringField('Account name', default="")
    iban = StringField('IBAN', default="")
    payment_mode = StringField('Payment mode', default="invoice")
    payment_term = StringField('Payment term', default="30d")
    postal_code = StringField('Postal code', default="")
    city = StringField('City', default="")
    country = StringField('Country', default="")
    service = StringField('Service', default="")
    building = StringField('Building', default="")
    street_number = StringField('Street number', default="")
    street_name = StringField('Street name', default="")
    street_extra = StringField('Street extra', default="")
    email = StringField('Email', default="")
    telephone = StringField('Telephone', default="")
    category = StringField('Customer category', default="") # professionnal, individual or community
    memo = StringField('Memo', default="")

class CapCustomer(CapCollection):
    """
    Capability of see customers (bill or quotation)
    """

    def iter_resources(self, objs, split_path):
        """
        Iter resources.

        Default implementation of this method is to return on top-level
        all customer.

        :param objs: type of objects to get
        :type objs: tuple[:class:`BaseObject`]
        :param split_path: path to discover
        :type split_path: :class:`list`
        :rtype: iter[:class:`BaseObject`]
        """
        if Customer in objs:
            self._restrict_level(split_path)

            return self.iter_customers()

    def iter_customers(self):
        """
        Iter pending payment.

        :rtype: iter[:class:`Customer`]
        """
        raise NotImplementedError()


